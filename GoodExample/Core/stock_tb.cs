﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoodExample.Core
{
    public class stock_tb
    {
        [Key]
        public int stock_id { get; set; }

        public string barCode { get; set; }

        public string product_des { get; set; }

        public IList<price_tb> prices { get; set; }

    }
}