﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoodExample.Core
{
    public class price_tb
    {
        [Key]
        public int price_id { get; set; }

        public int stock_id {get; set; }

        public Double? selling_price { get; set; }

        public Double? cost_price { get; set; }
        public Double? margin { get; set; }
    }
}