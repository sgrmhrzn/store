﻿using GoodExample.Core;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace GoodExample.Data
{
    public class AppDbContext : DbContext
    {
    //    public IDbSet<project_tb> projects { get; set; }
        public AppDbContext()   : base("AppDbContext")
        {

        }

        public DbSet<stock_tb> stocks { get; set; }

        public DbSet<price_tb> prices { get; set; }

        public DbSet<salesDetail_tb> sales { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            base.OnModelCreating(modelBuilder);
        }
    }


}